import logo from './logo.svg';
import './App.css';
import './fonts/font.css'
import Header from "./components/Header/Header";
import Aside from "./components/Aside/Aside";
import Main from "./components/Main/Main";

function App() {


  return (
    <div className={"App"}>
     <Aside />
     <div className={"catalog"}>
         <Header />
         <Main />
     </div>

        {/*<div className="loading">*/}
        {/*    <div className="loadingImg">*/}
        {/*        <img src="./images/transparent.png" />*/}
        {/*    </div>*/}
        {/*    <div className="loadingGif">*/}
        {/*        <img src="./images/load.gif" />*/}
        {/*    </div>*/}
        {/*</div>*/}

    {/*    <div className="sideMenu">*/}
    {/*        <div className="logo">*/}
    {/*            <img src="https://app.365dropship.com/assets/images/dropship_logo.png"/>*/}
    {/*        </div>*/}
    {/*        <p className="bar-icon"> <i className="fas fa-user-circle"></i></p>*/}
    {/*        <p className="bar-icon"> <i className="fas fa-tachometer-alt"></i></p>*/}
    {/*        <p className="bar-icon"> <i className="fas fa-bars"></i></p>*/}
    {/*        <p className="bar-icon"> <i className="fas fa-cube"></i></p>*/}
    {/*    <p className="bar-icon"> <i className="fas fa-shopping-cart"></i></p>*/}
    {/*    <p className="bar-icon"> <i className="fas fa-clipboard-check"></i></p>*/}
    {/*    <p className="bar-icon"> <i className="fas fa-arrows-alt-h"></i></p>*/}
    {/*    <p className="bar-icon"> <i className="fas fa-clipboard-list"></i></p>*/}
    {/*</div>*/}

    {/*<div className="sideBar">*/}

    {/*    <div className="sideBar__select">*/}
    {/*        <select className="choose" name="choose">*/}
    {/*            <option selected hidden>Choose Niche</option>*/}
    {/*            <option value="bestSellers"> Best Sellers</option>*/}
    {/*            <option value="beauty"> Beauty</option>*/}
    {/*            <option value="electronics"> Electronics</option>*/}
    {/*            <option value="fashion"> Fashion</option>*/}
    {/*            <option value="fragrances"> Fragrances</option>*/}
    {/*        </select>*/}
    {/*    </div>*/}
    {/*    <div className="sideBar__select-cathegory">*/}
    {/*        <select className="chooseCathegory" name="chooseCathegory">*/}
    {/*            <option selected hidden>Choose Category</option>*/}
    {/*            <option value="val"> ...</option>*/}
    {/*        </select>*/}
    {/*    </div>*/}
    {/*    <div className="sideBar__form">*/}
    {/*        <select className="sideBar__select-from">*/}
    {/*            <option selected hidden>Ship From</option>*/}
    {/*            <option id = "idk"> Australia </option>*/}
    {/*            <option>   China  </option>*/}
    {/*            <option>   Czech Republic  </option>*/}
    {/*            <option>    France </option>*/}
    {/*            <option>   Germany </option>*/}
    {/*            <option>  India </option>*/}
    {/*            <option>  Israel </option>*/}
    {/*            <option>  Italy </option>*/}
    {/*            <option>   Latvia </option>*/}
    {/*            <option>     Netherlands </option>*/}
    {/*            <option>    Poland </option>*/}
    {/*            <option>   Singapore </option>*/}
    {/*            <option>     Spain </option>*/}
    {/*            <option>     Sweden </option>*/}
    {/*            <option>     Ukraine </option>*/}
    {/*            <option>   United Kingdom </option>*/}
    {/*            <option>  United States </option>*/}
    {/*            <option>    Vanuatu </option>*/}
    {/*        </select>*/}
    {/*    </div>*/}

    {/*    <div className="sideBar__form">*/}
    {/*        <select className="sideBar__select-to">*/}
    {/*            <option selected hidden>Ship To</option>*/}

    {/*        </select>*/}
    {/*    </div>*/}

    {/*    <div className="sideBar__form">*/}
    {/*        <select className="sideBar__select-supplier">*/}
    {/*            <option selected hidden>Select Supplier</option>*/}
    {/*        </select>*/}
    {/*    </div>*/}
    {/*    <div className="title-price">*/}
    {/*        <p><i className="fas fa-tags"></i> PRICE RANGE</p>*/}
    {/*    </div>*/}
    {/*    <div className="slidecontainer">*/}
    {/*        <input type="range" min="1" max="4198" value="1" className="slider" id="minPriceSlide" onchange="minPriceRange(this.value);"/>*/}

    {/*    </div>*/}
    {/*    <div className="slidecontainer">*/}
    {/*        <input type="range" min="1" max="4198" value="4198" className="sliderMax" id="maxPriceSlide" onchange="maxPriceRange(this.value);"/>*/}

    {/*    </div>*/}
    {/*    <div className="price-range">*/}
    {/*        <div>*/}
    {/*            <span className="range-price">$</span>*/}
    {/*            <input type="text" id="minPrice" value="0"/>*/}
    {/*        </div>*/}
    {/*        <div>*/}
    {/*            <span className="range-price">$</span>*/}
    {/*            <input type="text" id="maxPrice" value="0"/>*/}
    {/*        </div>*/}
    {/*    </div>*/}
    {/*    <div className="title-profit">*/}
    {/*        <p><i className="fas fa-dollar-sign"></i> PROFIT RANGE</p>*/}
    {/*    </div>*/}
    {/*    <div className="slidecontainer">*/}
    {/*        <input type="range" min="3" max="98" value="1" className="slider" id="myRange" onchange="minRange(this.value);"/>*/}

    {/*    </div>*/}
    {/*    <div className="slidecontainer">*/}
    {/*        <input type="range" min="3" max="98" value="98" className="sliderMax" id="maxVal" onchange="maxRange(this.value);"/>*/}

    {/*    </div>*/}
    {/*    <div className="profit-range">*/}
    {/*        <div>*/}
    {/*            <span className="range-percent">%</span>*/}
    {/*            <input type="text" id="minRange" value="0"/>*/}
    {/*        </div>*/}
    {/*        <div>*/}
    {/*            <span className="range-percent">%</span>*/}
    {/*            <input type="text" id="maxRange" value="0"/>*/}
    {/*        </div>*/}

    {/*    </div>*/}

    {/*    <div className="reset-button">*/}

    {/*        <button>reset filter</button>*/}
    {/*    </div>*/}

    {/*</div>*/}
    {/*/!*lmlksmkf*!/*/}
    {/*    <main className="main">*/}
    {/*        <div className="search__bar">*/}
    {/*            <div className="search__bar-select">*/}
    {/*                <div className="select__button-1">*/}
    {/*                    <button onClick='selects()'>Select All</button>*/}
    {/*                </div>*/}
    {/*                <div id="select-number">*/}
    {/*                    <p>*/}
    {/*                        selected 0 out of 274,670 products*/}
    {/*                    </p>*/}
    {/*                </div>*/}
    {/*                <div className="select__button-2">*/}
    {/*                    <button onClick='deSelect()'>clear</button>*/}
    {/*                </div>*/}
    {/*            </div>*/}

    {/*            <div className="nav__search">*/}
    {/*                <i className="fas fa-search"></i>*/}
    {/*                <input type="text" id="search" placeholder="search..."/>*/}
    {/*                    <button id="seachButton">ADD TO INVENTORY</button>*/}
    {/*            </div>*/}
    {/*            <div className="question-mark">*/}
    {/*                <button>?</button>*/}
    {/*            </div>*/}
    {/*        </div>*/}

    {/*        <nav className="nav">*/}
    {/*            <div className="nav__sort">*/}
    {/*                <p><i className="fas fa-sort-amount-down"></i> Sort By: </p>*/}
    {/*                <select className="sort" name="sort">*/}
    {/*                    <option value="asc"> Price: Low to High</option>*/}
    {/*                    <option value="desc"> Price: High to Low</option>*/}
    {/*                </select>*/}
    {/*            </div>*/}
    {/*        </nav>*/}
    {/*        <div className="wrapper">*/}

    {/*            <section className="catalog">*/}
                    {/*<div className="roundedOne">*/}
                    {/*    <label className="container">*/}
                    {/*        <span className="checkmark"></span>*/}
                    {/*    <input className="c1" type="checkbox" name="checkbox" onChange="checkboxes()"/>*/}
                    {/*        <div className="catalog__product">*/}
                    {/*            <div className="catalog__photo">*/}
                    {/*                <img src="./images/loadProduct.gif"/>*/}
                    {/*            </div>*/}
                    {/*            <div className="catalog__title">*/}
                    {/*            </div>*/}
                    {/*            <div className="catalog__price">*/}
                    {/*                <div className="RRP">*/}
                    {/*                </div>*/}
                    {/*                <div className="Profit">*/}
                    {/*                </div>*/}
                    {/*                <div className="Cost">*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}


                    {/*</label>*/}
                    {/*</div>*/}
                    {/*<div className="roundedOne"><label className="container"><span className="checkmark"></span>*/}
                    {/*    <input className="c1" type="checkbox" name="checkbox" onChange="checkboxes()"/>*/}
                    {/*        <div className="catalog__product">*/}
                    {/*            <div className="catalog__photo">*/}
                    {/*                <img src="./images/loadProduct.gif"/>*/}
                    {/*            </div>*/}
                    {/*            <div className="catalog__title">*/}
                    {/*            </div>*/}
                    {/*            <div className="catalog__price">*/}
                    {/*                <div className="RRP">*/}
                    {/*                </div>*/}
                    {/*                <div className="Profit">*/}
                    {/*                </div>*/}
                    {/*                <div className="Cost">*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}


                    {/*</label>*/}
                    {/*</div>*/}
                    {/*<div className="roundedOne"><label className="container"><span className="checkmark"></span>*/}
                    {/*    <input className="c1" type="checkbox" name="checkbox" onChange="checkboxes()"/>*/}
                    {/*        <div className="catalog__product">*/}
                    {/*            <div className="catalog__photo">*/}
                    {/*                <img src="./images/loadProduct.gif"/>*/}
                    {/*            </div>*/}
                    {/*            <div className="catalog__title">*/}
                    {/*            </div>*/}
                    {/*            <div className="catalog__price">*/}
                    {/*                <div className="RRP">*/}
                    {/*                </div>*/}
                    {/*                <div className="Profit">*/}
                    {/*                </div>*/}
                    {/*                <div className="Cost">*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}


                    {/*</label>*/}
                    {/*</div>*/}
                    {/*<div className="roundedOne"><label className="container"><span className="checkmark"></span>*/}
                    {/*    <input className="c1" type="checkbox" name="checkbox" onChange="checkboxes()"/>*/}
                    {/*        <div className="catalog__product">*/}
                    {/*            <div className="catalog__photo" style=" justify-content: center;">*/}
                    {/*                <img src="./images/loadProduct.gif"/>*/}
                    {/*            </div>*/}
                    {/*            <div className="catalog__title">*/}
                    {/*            </div>*/}
                    {/*            <div className="catalog__price">*/}
                    {/*                <div className="RRP">*/}
                    {/*                </div>*/}
                    {/*                <div className="Profit">*/}
                    {/*                </div>*/}
                    {/*                <div className="Cost">*/}
                    {/*                </div>*/}
                    {/*            </div>*/}
                    {/*        </div>*/}


                    {/*</label>*/}
                    {/*</div>*/}
    {/*            </section>*/}
    {/*        </div>*/}

    {/*    </main>*/}
    {/*/!*fsfs*!/*/}

    </div>
  );
}

export default App;
