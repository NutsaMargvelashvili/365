import Nav from "./Nav/Nav"
import "./Aside.css"

const Aside = () => {
    return (
        <div className={"aside_wrapper"}>
            <aside className={"aside"}>

                <Nav />
                <div className={"catalog__filter"}>

                </div>
            </aside>
        </div>


    )
}

export default Aside;