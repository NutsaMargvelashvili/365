import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faAlignJustify} from '@fortawesome/free-solid-svg-icons'
const Sort = () => {
    return(
        <div className={"nav_sort"}>
                <p> <FontAwesomeIcon icon={faAlignJustify}/> Sort By: </p>
                <select className="sort" name="sort">
                    <option value="asc"> Price: Low to High</option>
                    <option value="desc"> Price: High to Low</option>
                </select>
        </div>

    );

}
export default Sort;