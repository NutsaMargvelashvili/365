import "./Header.css"
import Button from "./Button.js"
import Sort from "./Sort.js"
import Search from "./Search.js"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {faQuestion} from '@fortawesome/free-solid-svg-icons'
const Header = () => {
    return (
        <div className={"header_wrapper"}>
            <div className="Header">
                <Button title={"select all"}/>
                <div className={"selected__products-number"}>
                    <p>selected 0 out of 269,921 products</p>
                </div>
                <Search />
                <Button title={"add to inventory"} big/>
                <button className={"help__BTN"}>  <FontAwesomeIcon icon={faQuestion} /></button>
            </div>
            <div className={"Header__sort-filter"}>
                <Sort />
            </div>
        </div>

    )
}

export default Header;