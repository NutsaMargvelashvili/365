
const Button = (props) => {
    return(
            <button className={"button" + ((props.big ? " button--big" : ""))}>{props.title}</button>
        );

}
export default Button;