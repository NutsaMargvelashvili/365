import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
// import 'font-awesome/css/font-awesome.min.css';
const Search = () => {
    return(
        // <div>
        //     <input className={"search__products"} type="text" placeholder="search...      &#xf002;"/>
        //     {/*<FontAwesomeIcon icon={faSearch} />*/}
        // </div>
        <div className={'Header'}>
            <div className="Search">
                <input type={'text'} placeholder={'search...'} className={"Search__products"} />
                <button className={'Search__ICON'}>
                    {/*<img*/}
                    {/*    src={"https://cdn0.iconfinder.com/data/icons/very-basic-2-android-l-lollipop-icon-pack/24/search-512.png"}*/}
                    {/*    alt="Search"/>*/}
                    <FontAwesomeIcon icon={faSearch} />
                </button>
            </div>
        </div>

    );

}
export default Search;